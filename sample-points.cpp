#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>

using namespace std;
using namespace cv;

std::vector<std::vector<Vec3b>> hsv_clust;
std::vector<Vec3b> hsv_px;
Mat hsv_img;

void callback(int event,int x,int y,int flags,void* ud)
{
	if(event == EVENT_LBUTTONDOWN)
	{
		Vec3b hsv = hsv_img.at<Vec3b>(y,x);

		hsv_px.push_back(hsv);

		cout << "At x,y: (" << x << "," << y << ") -- ";
		cout << "HSV: " << int(hsv.val[0]) << "," << int(hsv.val[1]) << "," << int(hsv.val[2]) << endl;
	}
}

int main(int argc,char** argv)
{
	//Open webcam
	VideoCapture cam(0);
	if(!cam.isOpened())
	{
		cout << "Cannot open video camera" << endl;
		return 1;
	}

	//Set the mouse callback function
	namedWindow("BGR Video");
	setMouseCallback("BGR Video",callback,NULL);

	while(true)
	{
		Mat frame;
		bool success = cam.read(frame);

		if(!success)
		{
			cout << "Cannot read frame from video stream" << endl;
			break;
		}

		Mat bgr_img;
		flip(frame,bgr_img,1);
		cvtColor(bgr_img,hsv_img,CV_BGR2HSV);

		imshow("BGR Video",bgr_img);
		int key = waitKey(20);
		if(key == 1048603) break;
	}
	//Save the average color of each cluster
	ofstream outfile("color-samples.txt");
	for(int i = 0; i < hsv_px.size(); i++)
	{
		outfile << int(hsv_px[i].val[0]) << "\n";
		outfile << int(hsv_px[i].val[1]) << "\n";
		outfile << int(hsv_px[i].val[2]) << "\n";
	}
	outfile.close();
	cout << "Saved " << hsv_px.size() << " HSV sample points in color-samples.txt." << endl;

	return 0;
}
