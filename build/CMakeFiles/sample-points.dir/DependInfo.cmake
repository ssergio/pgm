# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/zero/Maestria-CC/Cuatr. Ene-May-2018/Modelos Graf. Prob./Proyecto/code/sample-points.cpp" "/home/zero/Maestria-CC/Cuatr. Ene-May-2018/Modelos Graf. Prob./Proyecto/code/build/CMakeFiles/sample-points.dir/sample-points.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  ".."
  "/home/zero/Gdrive/Code/workspace/VPDetector/include"
  "/home/zero/bgslibrary-master/package_bgs/pl"
  "/home/zero/bgslibrary-master/package_bgs"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
