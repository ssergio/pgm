#include <iostream>
#include <vector>
#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>

using namespace std;
using namespace cv;

cv::Point furthestPixel(Mat &img, float &dist)
{
	Mat half_sized_img;
	resize(img,half_sized_img,Size(img.cols/2,img.rows/2));

	Mat distance_img;
	distanceTransform(half_sized_img,distance_img,CV_DIST_L2,3);

	float max = 0;
	int x = 0;
	int y = 0;
	for(int i = 0; i < distance_img.rows; i++)
	{
		for(int j = 0; j < distance_img.cols; j++)
		{
			if(distance_img.at<float>(i,j) > max)
			{
				max = distance_img.at<float>(i,j);
				x = j;
				y = i;
			}
		}
	}

//Mat cl;
//cvtColor(img,cl,CV_GRAY2BGR);
//circle(cl,Point(x,y),int(max),Scalar(0,0,255));
//circle(cl,Point(x,y),5,Scalar(0,0,255));
//imshow("No down size",cl);
//imshow("Dist. img.",distance_img);
//waitKey();

	dist = 2*max;

	return Point(x*2,y*2);
}



int main(int argc, char** argv)
{
Mat img = imread("/home/zero/Maestria-CC/Cuatr. Ene-May-2018/Modelos Graf. Prob./Proyecto/code/hand.png",0);

threshold(img,img,200,255,THRESH_BINARY);

Mat cpy = img.clone();

vector<vector<Point>> contours;
vector<Vec4i> hierarchy;
findContours(cpy, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

vector<vector<Point>>hull(contours.size());
for(int i = 0; i < contours.size(); i++)
{
	convexHull(Mat(contours[i]),hull[i],false);
}


//Get center and radius
float radius;
Point center = furthestPixel(img,radius);


Mat res;
cvtColor(img,res,CV_GRAY2BGR);
for(int i = 0; i < hull.size(); i++)
{
	drawContours(res, hull, i, Scalar(128,128,0), 1, 8, vector<Vec4i>(), 0, Point());
	for(int j = 0; j < hull[i].size(); j++)
	{
		circle(res,hull[i][j],4,Scalar(0,0,255));
	}
}

//Print the center circle
circle(res,center,int(radius*1.4),Scalar(0,255,0));

imshow("convex hull",res);
waitKey();
destroyAllWindows();

return 0;
}
