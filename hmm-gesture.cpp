#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>
#include "SuBSENSE.h"
#include "VPDetector.hpp"

#define PI 3.14159265

using namespace std;
using namespace cv;

//Global variables
std::vector<Point> prev2_fingers;
Point prev2_center;
string prev2_sign;
std::vector<Point> prev_fingers;
Point prev_center;
string prev_sign;
std::vector<Point> prev_samp_fingers;
Point prev_samp_center;
string prev_samp_sign;
int gesture_counter;

std::vector<std::vector<Scalar>> readColorFile(int var)
{
	//Open color file
	ifstream infile("hand-color.txt");
	if(!infile.is_open()) return std::vector<std::vector<Scalar>>();

	//Read every color
	std::vector<std::vector<int>> cluster;
	std::vector<int> color;
	string line;
	while(getline(infile,line))
	{
		int c = stoi(line);
		if(c == -1)
		{
			cluster.push_back(color);
			color.clear();
		}
		else color.push_back(c);
	}
	cluster.push_back(color);
	infile.close();


	std::vector<std::vector<Scalar>> output;
	for(int i = 0; i < cluster.size(); i++)
	{
		//Store the color ranges in Scalars
		Scalar bgr_low, bgr_up, hsv_low, hsv_up;
		bgr_low.val[0] = saturate_cast<uchar>(cluster[i][0]-var);
		bgr_low.val[1] = saturate_cast<uchar>(cluster[i][1]-var);
		bgr_low.val[2] = saturate_cast<uchar>(cluster[i][2]-var);
		bgr_up.val[0] = saturate_cast<uchar>(cluster[i][0]+var);
		bgr_up.val[1] = saturate_cast<uchar>(cluster[i][1]+var);
		bgr_up.val[2] = saturate_cast<uchar>(cluster[i][2]+var);
		hsv_low.val[0] = saturate_cast<uchar>(cluster[i][3]-var);
		hsv_low.val[1] = saturate_cast<uchar>(cluster[i][4]-var);
		hsv_low.val[2] = saturate_cast<uchar>(cluster[i][5]-var);
		hsv_up.val[0] = saturate_cast<uchar>(cluster[i][3]+var);
		hsv_up.val[1] = saturate_cast<uchar>(cluster[i][4]+var);
		hsv_up.val[2] = saturate_cast<uchar>(cluster[i][5]+var);

		std::vector<Scalar> sub_output;
		sub_output.push_back(bgr_low);
		sub_output.push_back(bgr_up);
		sub_output.push_back(hsv_low);
		sub_output.push_back(hsv_up);

		output.push_back(sub_output);
	}

	return output;
}

std::vector<Scalar> readSampleFile(int h_var,int s_var,int v_var)
{
	//Open color file
	ifstream infile("color-samples.txt");
	if(!infile.is_open()) return std::vector<Scalar>();

	//Read every color
	std::vector<int> color;
	string line;
	while(getline(infile,line))
	{
		int c = stoi(line);
		color.push_back(c);
	}
	infile.close();

	std::vector<Scalar> output;
	int n_samples = color.size() / 3;
	for(int i = 0; i < n_samples; i++)
	{
		Scalar original_samp = Scalar(color[i*3],color[i*3+1],color[i*3+2]);
		Scalar low_samp = Scalar(saturate_cast<uchar>(color[i*3] - h_var),
					saturate_cast<uchar>(color[i*3+1] - s_var),
					saturate_cast<uchar>(color[i*3+2] - v_var));
		Scalar hi_samp = Scalar(saturate_cast<uchar>(color[i*3] + h_var),
					saturate_cast<uchar>(color[i*3+1] + s_var),
					saturate_cast<uchar>(color[i*3+2] + v_var));
		output.push_back(original_samp);
		output.push_back(low_samp);
		output.push_back(hi_samp);
	}

	return output;
}

cv::Point furthestPixel(Mat &img, float &dist)
{
	Mat half_sized_img;
	resize(img,half_sized_img,Size(img.cols/2,img.rows/2));

	Mat distance_img;
	distanceTransform(half_sized_img,distance_img,CV_DIST_L1,3);

	float max = 0;
	int x = 0;
	int y = 0;
	for(int i = 0; i < distance_img.rows; i++)
	{
		for(int j = 0; j < distance_img.cols; j++)
		{
			if(distance_img.at<float>(i,j) > max)
			{
				max = distance_img.at<float>(i,j);
				x = j;
				y = i;
			}
		}
	}

	dist = 2*max;

	return Point(x*2,y*2);
}

void shiftVector(Point p0, Point p1, float &theta, float &magn)
{
	float d_x = p1.x - p0.x;
	float d_y = p1.y - p0.y;
	int d_x_sign = ((d_x > 0) ? (1) : (-1));
	int d_y_sign = ((d_y > 0) ? (1) : (-1));

	//Compute the angle theta
	if(d_x*d_y == 0)
	{
		if(d_x == 0 && d_y_sign == 1) theta = 270;
		else if(d_x == 0 && d_y_sign == -1) theta = 90;
		else if(d_y == 0 && d_x_sign == 1) theta = 0;
		else if(d_y == 0 && d_x_sign == -1) theta = 180;
	}
	else
	{
		float ang = 0.0;
		d_x = abs(d_x);
		d_y = abs(d_y);
		ang = atan(d_y / d_x) * 180 / PI;

		if(d_x_sign > 0 && d_y_sign < 0) theta = ang;//1st quarter
		else if(d_x_sign < 0 && d_y_sign < 0) theta = 180 - ang;//2nd quarter
		else if(d_x_sign < 0 && d_y_sign > 0) theta = 180 + ang;//3rd quarter
		else if(d_x_sign > 0 && d_y_sign > 0) theta = 360 - ang;//4th quarter
	}

	//Compute the magnitud
	magn = float(sqrt(pow(d_x,2) + pow(d_y,2)));

	return;
}

string signDetection(Mat &img, Point center, float radius, std::vector<Point> &centroids, std::vector<int> &areas, std::vector<float> &angles)
{
	string output = "";

	//Check if there is a hand
	if(radius < 30.0)
	{
		output = "NONE";
		return output;
	}

	//Fingers segmentation
	Mat fing_img = img.clone();
	circle(fing_img,center,int(radius*1.4),Scalar(0),-1);

//debug
fing_img.copyTo(img);

	//Fingers characterization
	float min_fing = radius*radius*PI*0.016;
	labelBigBlobs(fing_img,int(min_fing));
	area_centroid(fing_img,centroids,areas);

	//Start classification processing
	if(centroids.size() == 0) output = "FIST";
	else
	{
		//Remove from the vector the blob that correspond to the wrist
		for(int i = 0; i < centroids.size(); i++)
		{
			if(centroids[i].y > center.y)
			{
				centroids.erase(centroids.begin() + i);
				areas.erase(areas.begin() + i);
				break;
			}
		}

		//Order the finger centroids from left to right
		std::vector<Point> c_copy(centroids);
		std::vector<int> a_copy(areas);
		centroids.clear();
		areas.clear();
		int n_fingers = c_copy.size();
		for(int i = 0; i < n_fingers; i++)
		{
			int l_index = -1;
			int most_left = 999999;
			for(int j = 0; j < c_copy.size(); j++)
			{
				if(c_copy[j].x < most_left)
				{
					most_left = c_copy[j].x;
					l_index = j;
				}
			}

			centroids.push_back(c_copy[l_index]);
			areas.push_back(a_copy[l_index]);

			c_copy.erase(c_copy.begin() + l_index);
			a_copy.erase(a_copy.begin() + l_index);
		}

		//Compute the angle from the hand center to each extended finger
		angles.clear();
		for(int i = 0; i < centroids.size(); i++)
		{
			float ang = 0.0;
			float magn = 0.0;
			shiftVector(center,centroids[i],ang,magn);
			angles.push_back(ang);
		}

		//Compute fingers' area ratio with respect to the main circle
		std::vector<float> a_ratios;
		for(int i = 0; i < areas.size(); i++)
		{
			float ar = float(areas[i]) / (radius*radius*PI);
			a_ratios.push_back(ar);
		}

		//Classification phase
		output = "UNKNOWN";
		if(centroids.size() == 0) output = "FIST";
		else if(centroids.size() == 1)
		{
			if(a_ratios[0] >= 0.055 && angles[0] < 130 && angles[0] > 70) output = "INDEX";
		}
		else if(centroids.size() == 2)
		{
			float middle_angle = (angles[0] + angles[1]) / 2;
			float abs_angle = abs(angles[0] - angles[1]);
			if(middle_angle > 110) output = "L-SIGN";
			else if(middle_angle < 110) output = "PEACE";
		}

	}

	return output;
}

int digiTheta(float theta)
{
	//Find to which of the angles multiples of 45 theta is closest to
	float min_diff = 9999.9;
	int min_index = -1;
	for(int i = 0; i < 8; i++)
	{
		float diff;
		if(i == 0)
		{
			float d360 = abs(theta - 360);
			if(d360 < theta) diff = d360;
			else diff = theta;
		}
		else diff = abs(theta - i*45);

		if(diff < min_diff)
		{
			min_diff = diff;
			min_index = i;
		}
	}

	//Meaning of min_index:
	//1: 0 or 360 degrees
	//2: 45 degrees
	//3: 90 degrees
	//4: 135 degrees
	//5: 180 degrees
	//6: 225 degrees
	//7: 270 degrees
	//8: 315 degrees
	return (min_index + 1);
}

int gestureSample(string &sign, Point &center, std::vector<Point> &centroids, float dist_tresh)
{
	//From the current sign and the previous 2, define the most voted sign
	std::vector<int> vote_cast(6,0);
	std::vector<string> sign_record;
	sign_record.push_back(prev2_sign);
	sign_record.push_back(prev_sign);
	sign_record.push_back(sign);
	for(int i = 0; i < sign_record.size(); i++)
	{
		if(sign_record[i] == "UNKNOWN") vote_cast[0] += 1;
		else if(sign_record[i] == "FIST") vote_cast[1] += 1;
		else if(sign_record[i] == "INDEX") vote_cast[2] += 1;
		else if(sign_record[i] == "L-SIGN") vote_cast[3] += 1;
		else if(sign_record[i] == "PEACE") vote_cast[4] += 1;
		else if(sign_record[i] == "NONE") vote_cast[5] += 1;
	}
	int max_vote = 0;
	int max_index = 0;
	for(int i = 0; i < vote_cast.size(); i++)
	{
		if(vote_cast[i] >= max_vote)
		{
			max_vote = vote_cast[i];
			max_index = i;
		}
	}

	//Most voted sign
	std::vector<Point> curr_samp_fingers;
	Point curr_samp_center;
	string curr_samp_sign;
	switch(max_index)
	{
		case 0:
		{
			prev_samp_sign = "UNKNOWN";
		}
		break;
		case 1:
		{
			curr_samp_sign = "FIST";
		}
		break;
		case 2:
		{
			curr_samp_sign = "INDEX";
		}
		break;
		case 3:
		{
			curr_samp_sign = "L-SIGN";
		}
		break;
		case 4:
		{
			curr_samp_sign = "PEACE";
		}
		break;
		case 5:
		{
			curr_samp_sign = "NONE";
		}
		break;
		default:
		break;
	}

	//Get the finger centroids & center of the most recent & voted sign
	if(curr_samp_sign == sign)
	{
		curr_samp_fingers = centroids;
		curr_samp_center = center;
	}
	else if(curr_samp_sign == prev_sign)
	{
		curr_samp_fingers = prev_fingers;
		curr_samp_center = prev_center;
	}

	//Function output variable
	int output = 34;

	//Check if the previous sampled sign is the the same as the current one or
	//if the current sample is UNKOWN
	if(curr_samp_sign != prev_samp_sign || curr_samp_sign == "UNKNOWN" || curr_samp_sign == "NONE")
	{
		//Save the current sample for next sample iteration
		prev_samp_fingers = curr_samp_fingers;
		prev_samp_center = curr_samp_center;
		prev_samp_sign = curr_samp_sign;

		//Return 34 if the previous and current sampled sign are not the same or
		//if the current sign is UNKNOWN
	}
	else
	{
		//Compute the shift vector between the prev. and current sampled signs
		float theta = 0.0;
		float magn = 0.0;
		shiftVector(prev_samp_center,curr_samp_center,theta,magn);

		//Determine if the distance traversed is significant
		if(magn > dist_tresh)
		{
			//Discretize the theta shift angle into one of 8 recognizible ones
			int discrete_angle = digiTheta(theta);

			//Define the observation for the HMM based on the discrete theta and the current sign
			if(curr_samp_sign == "FIST") output = discrete_angle; 
			else if(curr_samp_sign == "INDEX") output = discrete_angle + 8;
			else if(curr_samp_sign == "L-SIGN") output = discrete_angle + 16;
			else if(curr_samp_sign == "PEACE") output = discrete_angle + 24;
		}
		else output = 33;

		//Save the current sample for next sample iteration
		prev_samp_fingers = curr_samp_fingers;
		prev_samp_center = curr_samp_center;
		prev_samp_sign = curr_samp_sign;
	}

	return output;
}

void gestureSegmentation(bool &gestureActive,std::vector<int> &gestureSequence, int currentGesture, string &saveDir)
{
	if(!gestureActive && currentGesture < 33)
	{
		gestureSequence.push_back(currentGesture);
		gestureActive = true;
	}
	else if(gestureActive)
	{
		gestureSequence.push_back(currentGesture);

		//Check if the sequence ends with three elements < 1
		if(gestureSequence.size() > 3)
		{
			int l_index = gestureSequence.size() - 1;
			int ll_index = l_index - 1;
			int lll_index = ll_index - 1;

			//The gesture has been finished
			if(gestureSequence[l_index] >= 33 && gestureSequence[ll_index] >= 33 && gestureSequence[lll_index] >= 33)
			{
				//Print the sequence in terminal & save it in a file
				string file_name =  string("sequence-") + std::to_string(gesture_counter) + string(".txt");
				ofstream outfile(saveDir + string("/") + file_name);
				outfile << gestureSequence[0] << "\n";
				cout << ">> Saved gesture seq. of observations: [" << gestureSequence[0];
				for(int i = 1; i < lll_index; i++)
				{
					if(gestureSequence[i] == 34) continue;
					outfile << gestureSequence[i] << "\n";
					cout << "," << gestureSequence[i];
				}
				outfile.close();
				cout << "] in " + file_name << endl;


				//Reset the array of gestures & the state variable
				gestureSequence.clear();
				gestureActive = false;
				gesture_counter++;
			}
		}
	}
}

std::vector<string> split_str(string &s, const char& c)
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

void loadHMM(string table_file,std::vector<Mat> &A,std::vector<Mat> &B,std::vector<string> &Gestures)
{
	//Open the file that contains a list of the A and B matrices files
	//and the gesture they were trained to recognize
	ifstream hmm_table(table_file);
	if(!hmm_table.is_open()) return;

	//Read all the lines, there must 3 of them for each HMM
	std::vector<string> lines;
	string line;
	while(getline(hmm_table,line)) lines.push_back(line);
	if((lines.size() % 3) != 0)
	{
		cout << ">> Error reading models index file: each HMM must be defined with 2 matrix files & the gesture of that HMM.\n";
		A.clear();
		B.clear();
		Gestures.clear();
		return;
	}
	hmm_table.close();

	//For each model to be loaded, read its transition mat. A,
	//observation mat. B and its gesture name
	A.clear();
	B.clear();
	Gestures.clear();
	for(int i = 0; i < lines.size() / 3; i++)
	{
		//Load transition matrix A
		ifstream A_file(lines[i*3]);
		string a_row;
		std::vector<std::vector<double>> ma;
		while(getline(A_file,a_row))
		{
			std::vector<string> tokens = split_str(a_row,',');
			std::vector<double> elems;
			for(int j = 0; j < tokens.size(); j++) elems.push_back(stod(tokens[j]));
			ma.push_back(elems);
		}
		A_file.close();
		int n_rows_a = ma.size();
		int n_cols_a = ma[0].size();

		//Verify that every matrix A is a squared matrix
		if(n_rows_a != n_cols_a)
		{
			cout << ">> Error loading matrix A of HMM #" << i+1 << ": matrix A must be a squared matrix.\n";
			A.clear();
			B.clear();
			Gestures.clear();
			return;
		}

		Mat mat_A(n_rows_a,n_cols_a,CV_64F);
		for(int j = 0; j < n_rows_a; j++)
		{
			for(int k = 0; k < n_cols_a; k++) mat_A.at<double>(j,k) = ma[j][k];
		}
		A.push_back(mat_A);

		//Load observation matrix B
		ifstream B_file(lines[i*3+1]);
		string b_row;
		std::vector<std::vector<double>> mb;
		while(getline(B_file,b_row))
		{
			std::vector<string> tokens = split_str(b_row,',');
			std::vector<double> elems;
			for(int j = 0; j < tokens.size(); j++) elems.push_back(stod(tokens[j]));
			mb.push_back(elems);
		}
		B_file.close();
		int n_rows_b = mb.size();
		int n_cols_b = mb[0].size();

//debug
		//Verify that every matrix B has 33 columns and as many rows as its corresponding matrix A
		if((n_cols_b != 33 &&false) || n_rows_b != n_rows_a)
		{
			cout << ">> Error loading matrix B of HMM #" << i+1 << ": matrix B must have 33 columns and the same amount of rows as matrix A.\n";
			A.clear();
			B.clear();
			Gestures.clear();
			return;
		}

		Mat mat_B(n_rows_b,n_cols_b,CV_64F);
		for(int j = 0; j < n_rows_b; j++)
		{
			for(int k = 0; k < n_cols_b; k++) mat_B.at<double>(j,k) = mb[j][k];
		}
		B.push_back(mat_B);

		//Get the gesture this HMM recognizes
		Gestures.push_back(lines[i*3+2]);
	}
}

string forwardHMM(std::vector<Mat> &A,std::vector<Mat> &B,std::vector<string> &Gestures, std::vector<int> Obs)
{
	//Vector for saving the HMMs probabilities
	std::vector<double> hmm_prob;

	//Apply the forward algorithm for each HMM
	int n_obs = 33;
	int seq_len = Obs.size();
	for(int i = 0; i < Gestures.size(); i++)
	{
		//Get the amount of states this HMM has
		int n_states = A[i].rows;

		//Compute alphas for the first observation, time t=0
		std::vector<double> prev_alpha(n_states,0.0);
		std::vector<double> alpha;
		//prev_alpha[0] = B[i].at<double>(0,Obs[0]-1);
		prev_alpha[0] = 1;

		//Compute alphas for every state from time t=0 to time t=T-1, where T is seq_len
		for(int j = 0; j < seq_len; j++)
		{
			//Reset the alpha vector for the current iteration
			alpha.clear();

			//Compute alpha for each state
			for(int k = 0; k < n_states; k++)
			{
				double alpha_sum = 0.0;
				for(int l = 0; l < n_states; l++) alpha_sum = alpha_sum + (prev_alpha[l] * A[i].at<double>(l,k));
				alpha_sum = alpha_sum * B[i].at<double>(k,Obs[j]-1);

				alpha.push_back(alpha_sum);
			}

			//Save the current alphas for the next iteration
			prev_alpha = alpha;
		}

		//Compute the prob. of the whole sequence & save it
		double prob_sum = 0.0;
		for(int j = 0; j < n_states; j++) prob_sum = prob_sum + alpha[j];
		hmm_prob.push_back(prob_sum);
	}

	//Determine the winning HMM
	double max_prob = -1.0;
	int winner_index = -1;
	for(int i = 0; i < hmm_prob.size(); i++)
	{
		if(hmm_prob[i] > max_prob)
		{
			max_prob = hmm_prob[i];
			winner_index = i;
		}
	}

	//Return the winning HMM
	return Gestures[winner_index];
}

int main(int argc,char** argv)
{
	//Get sample period
	int sp = stoi(string(argv[1]));

	//Get significant traversed distance relative to the main circle radius
	float sig_dist_ratio = stof(string(argv[2]));

	//Get the directory where gestures will be saved
	string saveDir(argv[3]);

	//Get target color from file
	//[2 5 10] hand tweeked parameters
	std::vector<Scalar> hsv_samples = readSampleFile(2,5,10);

	//Open the webcam
	VideoCapture cam(0);
	if(!cam.isOpened())
	{
		cout << "Cannot open video camera" << endl;
		return 1;
	}

	//Initialize the timer
	bool time_to_sample = false;
	clock_t start_t = clock();

	//Initialize the sampling visual hint
	bool switch_color = false;

	//Initialize global variables
	prev2_fingers = std::vector<Point>();
	prev2_center = Point();
	prev2_sign = "NONE";
	prev_fingers = std::vector<Point>();
	prev_center = Point();
	prev_sign = "NONE";
	prev_samp_fingers = std::vector<Point>();
	prev_samp_center = Point();
	prev_samp_sign = "NONE";

	//Initialize the HMM observation variable
	int HMM_O = -1;

	//Initialize gesture array & state variable
	std::vector<int> gesture_seq;
	bool performing_gest = false;
	gesture_counter = 0;

	while(true)
	{
		Mat bgr_img,frame;
		bool success = cam.read(frame);

		if(!success)
		{
			cout << "Cannot read frame from video stream" << endl;
			break;
		}

		//Measure elapsed time since last sampling instant
		clock_t now_t = clock();
		double elapsed_msec = 1000 * (double(now_t - start_t) / CLOCKS_PER_SEC);
		if(elapsed_msec >= sp)
		{
			switch_color = !switch_color;
			time_to_sample = true;
			start_t = clock();
		}


		//Mirror the image
		flip(frame,bgr_img,1);

		//Process the image
		Mat bgr_range, hsv_range, hsv_img;
		Mat or_img = Mat::zeros(bgr_img.rows,bgr_img.cols, CV_8UC1);
		cvtColor(bgr_img,hsv_img,CV_BGR2HSV);

		//Color segmentation
		int n_samples = hsv_samples.size() / 3;
		for(int i = 0; i < n_samples; i++)
		{
			inRange(hsv_img,hsv_samples[i*3+1],hsv_samples[i*3+2],hsv_range);
			bitwise_or(hsv_range,or_img,or_img);
		}
		Mat mask = Mat::zeros(hsv_img.rows+2,hsv_img.cols+2,CV_8UC1);
		for(int i = 0; i < or_img.rows; i++)
		{
			for(int j = 0; j < or_img.cols; j++)
			{
				int or_val = or_img.at<uchar>(i,j);
				int mask_val = mask.at<uchar>(i+1,j+1);

				if(or_val > 0 && mask_val == 0)
				{
					Mat temp_mask = Mat::zeros(hsv_img.rows+2,hsv_img.cols+2,CV_8UC1);
					//[4 10 10] hand tweeked parameters
					floodFill(hsv_img,temp_mask,Point(j,i),Scalar(255),0,Scalar(4,10,10),Scalar(4,10,10),4 | ( 255 << 8 ) | FLOODFILL_MASK_ONLY);
					bitwise_or(temp_mask,mask,mask);
				}
			}
		}

		//Morphologic operations
		dilate(mask,mask,Mat(),Point(-1,-1),5);

		//Fill holes in the hand blob
		threshold(mask,mask,10,255,THRESH_BINARY);
		Mat or_cp = mask.clone();
		floodFill(or_cp,Point(0,0),Scalar(255));
		bitwise_not(or_cp,or_cp);
		bitwise_or(or_cp,mask,mask);

		//Detect furthest pixel from a 0 pixel
		float radius = 0;
		Point center = furthestPixel(mask,radius);

//debug
Mat bef_h = mask.clone();

		//Hand sign classification
		std::vector<Point> centroids;
		std::vector<int> areas;
		std::vector<float> angles;
		string sign = signDetection(mask,center,radius,centroids,areas,angles);

		//Sample a gesture observation if possible

		if(time_to_sample)
		{
			time_to_sample = false;
			float min_significant_distance = sig_dist_ratio * radius;
			HMM_O = gestureSample(sign,center,centroids,min_significant_distance);

			//Detect when a gesture starts & ends
			gestureSegmentation(performing_gest,gesture_seq,HMM_O,saveDir);
		}

		//Update the signs detected in the las 3 frames
		prev2_fingers = prev_fingers;
		prev2_center = prev_center;
		prev2_sign = prev_sign;
		if(sign == "NONE" || sign == "UNKNOWN")
		{
			prev_fingers.clear();
			prev_center = Point();
			prev_sign = sign;
		}
		else
		{
			prev_fingers = centroids;
			prev_center = center;
			prev_sign = sign;
		}

		//Display the original & output images
		Mat disp;
		cvtColor(mask,disp,CV_GRAY2BGR);
		//Draw the main circle
		circle(disp,center,int(radius),Scalar(0,0,255));
		//Draw the sampling hint
		Scalar hint_color;
		if(switch_color) hint_color = Scalar(255,0,0);
		else hint_color = Scalar(0,0,255);
		circle(disp,Point(disp.cols - 40,40),30,hint_color,-1);

		//Draw the prev2, prev and current sign
		putText(disp,string("prev2: ") + prev2_sign,Point(0,40),FONT_HERSHEY_SIMPLEX,1,Scalar(0,255,0),2);
		putText(disp,string("prev: ") + prev_sign,Point(0,80),FONT_HERSHEY_SIMPLEX,1,Scalar(0,255,0),2);
		putText(disp,string("sign: ") + sign,Point(0,120),FONT_HERSHEY_SIMPLEX,1,Scalar(0,255,0),2);

		//Draw the prev_sample
		putText(disp,string("prev. sample: ") + prev_samp_sign,Point(0,180),FONT_HERSHEY_SIMPLEX,1,Scalar(255,0,0),2);
		//Draw the last HMM observation
		putText(disp,string("HMM Obs: ") + std::to_string(HMM_O),Point(0,220),FONT_HERSHEY_SIMPLEX,1,Scalar(0,0,255),2);

		//Draw lines from the center to each finger
		for(int i = 0; i < centroids.size(); i++)
		{
			circle(disp,centroids[i],10,Scalar(0,0,255));
			line(disp,center,centroids[i],Scalar(255,0,0));
		}

		//Display process info to the user
		imshow("Video Camera",bgr_img);
		imshow("OR in range",disp);
		imshow("Segmentation",bef_h);

		if(waitKey(15) > 0) break;
	}

	destroyAllWindows();

	return 0;
}
