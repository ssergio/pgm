
cmake_minimum_required(VERSION 2.8.12.2)
project(testtest)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# Initialize CXXFLAGS.
#set(CMAKE_CXX_FLAGS "-Wall -std=c++11")
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "-std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "-std=c++0x")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

find_package(OpenCV REQUIRED)

set(SOURCES 
  test.cpp
  #/home/zero/Gdrive/Code/workspace/VPDetector/source/VPDetector.cpp
  #/home/zero/bgslibrary-master/package_bgs/pl/SuBSENSE.cpp 
  #/home/zero/bgslibrary-master/package_bgs/pl/BackgroundSubtractorSuBSENSE.cpp 
  #/home/zero/bgslibrary-master/package_bgs/pl/BackgroundSubtractorLBSP.cpp 
  #/home/zero/bgslibrary-master/package_bgs/pl/LBSP.cpp
)

set(DIRS 
  ${CMAKE_SOURCE_DIR}
  #/home/zero/Gdrive/Code/workspace/VPDetector/include
  ${OpenCV_INCLUDE_DIRS}
  #/home/zero/bgslibrary-master/package_bgs/pl 
  #/home/zero/bgslibrary-master/package_bgs
)

include_directories(${DIRS})
link_directories(/usr/lib/x86_64-linux-gnu)


set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
add_executable(tt ${SOURCES})
target_link_libraries(tt ${OpenCV_LIBS})
