

clc
clear

A = [.5 .5;.5 .5];
B = [.8 .2;.2 .8];
P = [.5 .5];

A = [0 P; zeros(size(A,1),1) A];
B = [zeros(1,size(B,2)); B];

%      H H T T
seq = [1 1 2 2];

[pstates,logpseq,forw,back,S] = hmmdecode(seq,A,B);

seq
fprintf('P(seq | model) = %.5f\n',exp(logpseq))