#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>

using namespace std;
using namespace cv;


std::vector<std::vector<Vec3b>> bgr_clust;
std::vector<std::vector<Vec3b>> hsv_clust;
std::vector<Vec3b> bgr_px;
std::vector<Vec3b> hsv_px;
Mat bgr_i;
Mat hsv_i;

void callback(int event,int x,int y,int flags,void* ud)
{
	if(event == EVENT_LBUTTONDOWN)
	{
		Vec3b bgr = bgr_i.at<Vec3b>(y,x);
		Vec3b hsv = hsv_i.at<Vec3b>(y,x);

		bgr_px.push_back(bgr);
		hsv_px.push_back(hsv);

		cout << "At x,y: (" << x << "," << y << ")" << endl;
		cout << "BGR: " << int(bgr.val[0]) << "," << int(bgr.val[1]) << "," << int(bgr.val[2]) << endl;
		cout << "HSV: " << int(hsv.val[0]) << "," << int(hsv.val[1]) << "," << int(hsv.val[2]) << endl;
	}
}

int main(int argc, char** argv)
{
	//For selecting a specific camera
	string cam_index = string(argv[1]);
	int ci = stoi(cam_index);	

	//Open webcam
	VideoCapture cam(ci);
	if(!cam.isOpened())
	{
		cout << "Cannot open video camera" << endl;
		return 1;
	}

	//Set the mouse callback function
	namedWindow("BGR Video");
	setMouseCallback("BGR Video",callback,NULL);

	cout << ">> Start sampling for cluster " << bgr_clust.size()+1 << "...\n";
	while(true)
	{
		bool success = cam.read(bgr_i);

		if(!success)
		{
			cout << "Cannot read frame from video stream" << endl;
			break;
		}

		cvtColor(bgr_i,hsv_i,CV_BGR2HSV);

		imshow("BGR Video",bgr_i);

		//Esc: 1048603
		//Space: 1048608
		int key = waitKey(20);
		if(key == 1048603)
		{
			if(bgr_px.size() > 0)
			{
				bgr_clust.push_back(bgr_px);
				hsv_clust.push_back(hsv_px);
			}
			cout << ">> Stopped sampling" << endl;
			break;
		}
		else if(key == 1048608)
		{
			bgr_clust.push_back(bgr_px);
			hsv_clust.push_back(hsv_px);
			bgr_px.clear();
			hsv_px.clear();
			cout << ">> Start sampling for cluster " << bgr_clust.size()+1 << "...\n";
		}

	}

	destroyAllWindows();

	//Save the average color of each cluster
	ofstream outfile("hand-color.txt");
	for(int i = 0; i < bgr_clust.size(); i++)
	{
		int b = 0, g = 0, r = 0;
		int h = 0, s = 0, v = 0;
		int num_samps = bgr_clust[i].size();
		for(int j = 0; j < num_samps; j++)
		{
			b += int(bgr_clust[i][j].val[0]);
			g += int(bgr_clust[i][j].val[1]);
			r += int(bgr_clust[i][j].val[2]);
			h += int(hsv_clust[i][j].val[0]);
			s += int(hsv_clust[i][j].val[1]);
			v += int(hsv_clust[i][j].val[2]);
		}
		b /= num_samps;
		g /= num_samps;
		r /= num_samps;
		h /= num_samps;
		s /= num_samps;
		v /= num_samps;

		cout << "Number of samples in cluster "<< i+1 <<": " << num_samps << endl;
		cout << "BGR avg: " << b << "," << g << "," << r << endl;
		cout << "HSV avg: " << h << "," << s << "," << v << endl;

		outfile << b << "\n";
		outfile << g << "\n";
		outfile << r << "\n";
		outfile << h << "\n";
		outfile << s << "\n";
		outfile << v << "\n";

		//Marker separating clusters
		if(i < bgr_clust.size()-1) outfile << -1 << "\n";
	}
	outfile.close();
	cout << "Saved BGR and HSV data in hand-color.txt." << endl;

	return 0;
}
