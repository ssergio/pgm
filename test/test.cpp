#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>

using namespace std;
using namespace cv;

std::vector<string> split_str(string &s, const char& c)
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

int main(int argc, char** argv)
{
string ss("sergio,arredondo,serrano,es,la,verga");
std::vector<string> tokens = split_str(ss,',');
for(int i = 0; i < tokens.size(); i++) cout << tokens[i] << endl;
return 0;

string f_name(argv[1]);
ifstream infile(f_name);

if(infile.is_open())
{
string line;
while(getline(infile,line))
{
cout << line << "|";
}
infile.close();
}

	return 0;
}
